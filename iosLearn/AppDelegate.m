//
//  AppDelegate.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/9.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate ()<UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSLog(@"--- %s ---",__func__);//__func__打印方法名
    NSLog(@"%@",[NSRunLoop currentRunLoop]);
    
    
    if (@available(iOS 10.0, *)) {
        //iOS10之后的注册方法
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate=self;
        UNAuthorizationOptions types=UNAuthorizationOptionBadge|UNAuthorizationOptionAlert|UNAuthorizationOptionSound;
        [center requestAuthorizationWithOptions:types     completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if (granted) {
                //点击允许
                //这里可以添加一些自己的逻辑
                NSLog(@"允许通知");
            } else {
                //点击不允许
                //这里可以添加一些自己的逻辑
                NSLog(@"不允许通知,请重装");
            }
        }];
    }else{
        //iOS8~iOS10的注册方法
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    //注册远端消息通知获取device token
    [application registerForRemoteNotifications];
    return YES;
}


#pragma mark - UISceneSession lifecycle


- (UISceneConfiguration *)application:(UIApplication *)application configurationForConnectingSceneSession:(UISceneSession *)connectingSceneSession options:(UISceneConnectionOptions *)options {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return [[UISceneConfiguration alloc] initWithName:@"Default Configuration" sessionRole:connectingSceneSession.role];
}


- (void)application:(UIApplication *)application didDiscardSceneSessions:(NSSet<UISceneSession *> *)sceneSessions {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    //收到推送的内容
    UNNotificationContent *content = notification.request.content;
    // 推送消息的标题
    NSString *title = content.title;
    // 推送消息的副标题
    NSString *subtitle = content.subtitle;
    if ([notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]){
        
    }else if ([notification.request.trigger isKindOfClass:[UNTimeIntervalNotificationTrigger class]]) {
        NSLog(@"%@",title);
    }else if ([notification.request.trigger isKindOfClass:[UNCalendarNotificationTrigger class]]){
        NSLog(@"%@",subtitle);
    }
    completionHandler(UNNotificationPresentationOptionBadge|
                      UNNotificationPresentationOptionSound|
                      UNNotificationPresentationOptionAlert);
    
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    //收到推送的内容
    UNNotificationContent *content = response.notification.request.content;
    // 推送消息的标题
    NSString *title = content.title;
    // 推送消息的副标题
    NSString *subtitle = content.subtitle;
    if ([response.notification.request.trigger isKindOfClass:[UNPushNotificationTrigger class]]){
        
    }else if ([response.notification.request.trigger isKindOfClass:[UNTimeIntervalNotificationTrigger class]]) {
        NSLog(@"%@",title);
    }else if ([response.notification.request.trigger isKindOfClass:[UNCalendarNotificationTrigger class]]){
        NSLog(@"%@",subtitle);
    }
    completionHandler();
}

@end
