//
//  CommonViewController.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/7/27.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "CommonViewController.h"
#import "CarFactory.h"
#import "Car.h"


@interface CommonViewController ()

@end

@implementation CommonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
  
      
      [self factoryModel];
}


//工厂模式
- (void)factoryModel{
    CarFactory *factory = [[CarFactory alloc] carWithFactoryType:BenzCarFactory];
    Car *car = [factory creatCar];
    [car carName];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
