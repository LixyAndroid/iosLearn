//
//  CollectionView.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/10.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#define JkScreenHeight [UIScreen mainScreen].bounds.size.height
#define JkScreenWidth [UIScreen mainScreen].bounds.size.width


#import "CollectionViewController.h"
#import "YiRefreshHeader.h"
#import "CollectionViewModel.h"
#import "ProductsModel.h"
#import "ProductsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSString* SectionCellID = @"SectionCellID";

@interface CollectionViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) YiRefreshHeader *refreshHeader;
@property (strong, nonatomic) CollectionViewModel *collectionViewModel;

@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;

@property (nonatomic, strong) UICollectionView *collectionView;


@end

@implementation CollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupUI];
}


- (void) setupUI{
    self.title = @"CollectionView";
    
    self.collectionViewModel=[[CollectionViewModel alloc] init];
    self.entries=[[NSMutableArray alloc] init ];
    
    //    YiRefreshHeader  头部刷新按钮的使用  下啦刷新
    self.refreshHeader=[[YiRefreshHeader alloc] init];
    self.refreshHeader.scrollView=self.collectionView;
    [self.refreshHeader header];
    __weak typeof(self) weakSelf = self;
    self.refreshHeader.beginRefreshingBlock=^(){
        __strong typeof(self) strongSelf = weakSelf;
        [strongSelf headerRefreshAction];
    };
    
    //    是否在进入该界面的时候就开始进入刷新状态
    [self.refreshHeader beginRefreshing];
    
    [self.view addSubview:self.collectionView];
    
    
}


- (void)headerRefreshAction
{
    [self.collectionViewModel headerRefreshRequestWithCallback:^(NSArray *array){
        self.entries=array;
        [self.refreshHeader endRefreshing];
        [self.collectionView reloadData];
        
        
    }];
    
}


#pragma mark -- collectionView

- (UICollectionView *)collectionView
{
    UICollectionViewFlowLayout *flowlayout = [[UICollectionViewFlowLayout alloc] init];
    
    //设置滚动方向
    [flowlayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    //左右间距
    flowlayout.minimumInteritemSpacing = JkScreenWidth*0.01;
    //上下间距
    flowlayout.minimumLineSpacing = 10;
    //flowlayout.sectionInset = UIEdgeInsetsMake(<#CGFloat top#>, <#CGFloat left#>, <#CGFloat bottom#>, <#CGFloat right#>)
    flowlayout.sectionInset = UIEdgeInsetsMake(0, 6, 0, 6);
    if (!_collectionView) {
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0 , 0 , JkScreenWidth, JkScreenHeight) collectionViewLayout:flowlayout];
    }
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    
    [_collectionView setBackgroundColor:[UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1/1.0]];
    _collectionView.showsVerticalScrollIndicator = NO;
    
    //注册xib
    [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([ProductsCell class]) bundle:nil] forCellWithReuseIdentifier:SectionCellID];
    
    
    return _collectionView;
}


#pragma mark ---------   配置单元格   --------------
//配置单元格
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger nodeCount = self.entries.count;
    ProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:SectionCellID forIndexPath:indexPath];
    
    if (nodeCount>0) {
        
        ProductsModel *productsModel = (self.entries)[indexPath.row];
        
        NSURL *imgUrl = [NSURL URLWithString:productsModel.thumbnailUrl];
        
        [cell.smallImageView sd_setImageWithURL:imgUrl placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        NSString *string;
        
        string = [NSString stringWithFormat:@"%@", productsModel.title ];
        cell.titleLabel.text = string;
    }
    
    
    return cell;
    
}
#pragma mark ---------  UICollectionView  Delegate/DataSource-----
//点击单元格
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"别点我");
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}
// 配置区头
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        
        UIView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"topView" forIndexPath:indexPath];
        
        headerView.backgroundColor = [UIColor blackColor];
        
        return headerView;
        
    }
    return nil;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSUInteger count = self.entries.count;
    if (count==0) {
        return 1;
    }
    return count;
}
//设置区头高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return CGSizeZero;
    }
    return CGSizeMake(JkScreenWidth,50);
}

////设置区尾高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    return CGSizeZero;
}


////设置cell大小
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(JkScreenWidth*0.47,JkScreenHeight*0.4);
}


@end
