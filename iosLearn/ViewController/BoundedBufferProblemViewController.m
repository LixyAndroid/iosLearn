//
//  BoundedBufferProblemViewController.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/7/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "BoundedBufferProblemViewController.h"

@interface BoundedBufferProblemViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSCondition *condition;
@property (strong) NSMutableArray *taskArr;
@property (strong,nonatomic) UITableView *tableView;

@end

static NSString *identifier = @"identifier";

@implementation BoundedBufferProblemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.taskArr = [[NSMutableArray alloc] initWithCapacity:42];
    self.condition = [[NSCondition alloc] init];
    
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    //去除多余的横线
    tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:identifier];
    
    
    //创建消费者队列
    dispatch_queue_t coustomerQueue = dispatch_queue_create("coustomerQueue", NULL);
    dispatch_async(coustomerQueue, ^{
        //消费者会一直去销毁东西
        while (1) {
            //这里加锁是必要的
            [self.condition lock];
            
            //当产品队列是空时，线程等待
            while ([self.taskArr count] == 0) {
                [self.condition wait];
            }
            
            
            //取出产品
            id task = [self.taskArr firstObject];
            
            if (task) {
                //销毁产品
                [self.taskArr removeObject:task];
                NSLog(@"customer eat task count = %lu",(unsigned long)[self.taskArr count]);
            }
            [self.condition unlock];
            
        }
        
    });
    
    
    dispatch_queue_t factoryQueue = dispatch_queue_create("factoryQueue", NULL);
    //生产者会一直尝试去生产产品
    dispatch_async(factoryQueue, ^{
        
        while (1) {
            [self.condition lock];
            [self.taskArr addObject:@"a"];
            /*
             %p表示输出这个指针,
             %d表示后面的输出类型为有符号的10进制整形，
             %u表示无符号10进制整型，
             %lu表示输出无符号长整型整数 (long unsigned)
             */
            NSLog(@"factory create task count = %lu", (unsigned long)[self.taskArr count]);
            //生产完毕后会告诉等待线程
            [self.condition signal];
            [self.condition unlock];
        }
        
    });
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        
        //初始化cell
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%zd",indexPath.row];
    return cell;
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
}

@end
