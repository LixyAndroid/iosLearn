//
//  CollectionView.h
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/10.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "VSBaseController.h"
NS_ASSUME_NONNULL_BEGIN

@interface CollectionViewController : VSBaseController
@property (strong, nonatomic) NSArray *entries;
@property (nonatomic, assign) UICollectionViewScrollDirection scrollDirection;  //滚动方向

@end

NS_ASSUME_NONNULL_END
