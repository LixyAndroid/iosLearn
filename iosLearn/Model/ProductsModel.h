//
//  ProductsModel.h
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/10.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductsModel : NSObject


@property (strong, nonatomic) NSNumber *albumId;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *thumbnailUrl;

@end

NS_ASSUME_NONNULL_END
