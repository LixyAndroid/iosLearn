//
//  ProductsCell.h
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/10.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProductsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *smallImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

NS_ASSUME_NONNULL_END
