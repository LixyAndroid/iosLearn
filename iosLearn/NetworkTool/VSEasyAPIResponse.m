//
//  VSEasyAPIResponse.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/7/27.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "VSEasyAPIResponse.h"

inline NSNumber *VSEasyAPIResponseEnvelopeCode(id responseObject) {
    if ([responseObject respondsToSelector:@selector(code)]) {
        return [(VSEasyAPIResponse *)responseObject code];
    } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
        id code = responseObject[@"code"];
        if (code) {
            return @([[NSString stringWithFormat:@"%@", code] integerValue]);
        }
    }
    return nil;
}

inline id VSEasyAPIResponseEnvelopeData(id responseObject) {
    if ([responseObject respondsToSelector:@selector(data)]) {
        return [(VSEasyAPIResponse *)responseObject data];
    } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
        return responseObject[@"data"];
    }
    return nil;
}

inline NSString *VSEasyAPIResponseEnvelopeMsg(id responseObject) {
    if ([responseObject respondsToSelector:@selector(msg)]) {
        return [(VSEasyAPIResponse *)responseObject msg];
    } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
        return responseObject[@"msg"];
    }
    return nil;
}

inline NSString *VSEasyAPIResponseEnvelopeOriginalCode(id responseObject) {
    if ([responseObject respondsToSelector:@selector(originalCode)]) {
        return [(VSEasyAPIResponse *)responseObject originalCode];
    } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
        id originalCode = responseObject[@"originalCode"];
        if (originalCode) {
            return [NSString stringWithFormat:@"%@", originalCode];
        }
    }
    return nil;
}

inline NSString *VSEasyAPIResponseEnvelopeDetailMsg(id responseObject) {
    if ([responseObject respondsToSelector:@selector(detailMsg)]) {
        return [(VSEasyAPIResponse *)responseObject detailMsg];
    } else if ([responseObject isKindOfClass:[NSDictionary class]]) {
        return responseObject[@"detailMsg"];
    }
    return nil;
}

inline id VSEasyAPIResponseBizModel(id responseObject) {
    if (![responseObject respondsToSelector:@selector(bizModel)]) {
        return nil;
    }
    id bizModel = [(VSEasyAPIResponse *)responseObject bizModel];
    if ([bizModel isKindOfClass:[NSArray class]]) {
        return [(NSArray *)bizModel firstObject];
    }
    return bizModel;
}

inline NSArray *VSEasyAPIResponseBizModelArray(id responseObject) {
    if (![responseObject respondsToSelector:@selector(bizModel)]) {
        return nil;
    }
    id bizModel = [(VSEasyAPIResponse *)responseObject bizModel];
    if (![bizModel isKindOfClass:[NSArray class]]) {
        return (bizModel ? @[bizModel] : nil);
    }
    return bizModel;
}

@implementation VSEasyAPIResponse

@end
