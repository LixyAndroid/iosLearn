//
//  VSEasyAPIResponse.h
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/7/27.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

extern NSNumber *VSEasyAPIResponseEnvelopeCode(id responseObject);
extern id VSEasyAPIResponseEnvelopeData(id responseObject);
extern NSString *VSEasyAPIResponseEnvelopeMsg(id responseObject);

extern NSString *VSEasyAPIResponseEnvelopeOriginalCode(id responseObject);
extern NSString *VSEasyAPIResponseEnvelopeDetailMsg(id responseObject);

extern id VSEasyAPIResponseBizModel(id responseObject);
extern NSArray *VSEasyAPIResponseBizModelArray(id responseObject);

@interface VSEasyAPIResponse : NSObject

@property (nonatomic, strong) NSNumber *code;
@property (nonatomic, strong) id data;
@property (nonatomic, copy) NSString *msg;

@property (nonatomic, copy) NSString *originalCode;
@property (nonatomic, copy) NSString *detailMsg;

@property (nonatomic, strong) id bizModel;

@end

NS_ASSUME_NONNULL_END
