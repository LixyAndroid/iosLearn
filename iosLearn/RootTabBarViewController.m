//
//  RootTabBarViewController.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/7/28.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "RootTabBarViewController.h"
#import "ViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"

@interface RootTabBarViewController ()

@property (nonatomic) NSInteger indexFlag;

@end

@implementation RootTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.indexFlag = 0;
    
    [self setupViewControllers];
}

/*
 大家应该都知道图片UIImage有个渲染模式

 UIImageRenderingMode枚举值来设置图片的renderingMode属性。该枚举中包含下列值：

 UIImageRenderingModeAutomatic  // 根据图片的使用环境和所处的绘图上下文自动调整渲染模式。

 UIImageRenderingModeAlwaysOriginal  // 始终绘制图片原始状态，不使用Tint Color。

 UIImageRenderingModeAlwaysTemplate  // 始终根据Tint Color绘制图片，忽略图片的颜色信息。
 */
- (void)setupViewControllers {
    ViewController *homeViewController = [[ViewController alloc] init];
    UIViewController *homeNavigationController = [[UINavigationController alloc]
                                                  initWithRootViewController:homeViewController];
    
    UITabBarItem *tabBarItem = [[UITabBarItem alloc] initWithTitle:@"首页" image:[[UIImage imageNamed:@"tabbar_home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabbar_home_selected_os7"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    tabBarItem.imageInsets = UIEdgeInsetsMake(-2, 0, 2, 0);
    tabBarItem.titlePositionAdjustment = UIOffsetMake(0, -3);
    
    [homeNavigationController setTabBarItem:tabBarItem];
    
    SecondViewController *secondViewController = [[SecondViewController alloc] init];
    UIViewController *secondNavigationController = [[UINavigationController alloc]
                                                    initWithRootViewController:secondViewController];
    [secondNavigationController setTabBarItem:[[UITabBarItem alloc] initWithTitle:@"购物" image:[[UIImage imageNamed:@"tabbar_home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabbar_home_selected_os7"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]]];
    
    ThirdViewController *thirdViewController = [[ThirdViewController alloc] init];
    UIViewController *thirdNavigationController = [[UINavigationController alloc]
                                                   initWithRootViewController:thirdViewController];
    [thirdNavigationController setTabBarItem:[[UITabBarItem alloc] initWithTitle:@"我的" image:[[UIImage imageNamed:@"tabbar_home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[[UIImage imageNamed:@"tabbar_home_selected_os7"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]]];
    
    
    NSArray *viewControllers = @[
        homeNavigationController,
        secondNavigationController,
        thirdNavigationController
    ];
    self.viewControllers = viewControllers;
    
    //选中的颜色
    self.tabBar.tintColor = [UIColor orangeColor];
    
    //这里也可以设置
//        [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor purpleColor]} forState:UIControlStateNormal];
//        [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName: [UIColor orangeColor]} forState:UIControlStateSelected];
    
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    NSInteger index = [self.tabBar.items indexOfObject:item];
    
    if (self.indexFlag != index) {
        [self animationWithIndex:index];
    }
    
}
// 动画
- (void)animationWithIndex:(NSInteger) index {
    NSMutableArray * tabbarbuttonArray = [NSMutableArray array];
    for (UIView *tabBarButton in self.tabBar.subviews) {
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [tabbarbuttonArray addObject:tabBarButton];
        }
    }
    
    UIView *animationView = tabbarbuttonArray[index];
    
    
    
    UIImageView *imageView;
    for (UIView *sub in animationView.subviews) {
        if ([sub isKindOfClass:[UIImageView class]]) {
            
            imageView = (UIImageView *)sub;
        }
    }
    
    //缩放
    //[self scaleAnimationWithImageView:imageView];
    
    //抖动
    [self shakeImageWithImageView:imageView];
    
    //脉冲
    //[self pulseAnimation:imageView];
    self.indexFlag = index;
    
}

//脉冲
-(void)pulseAnimation:(UIImageView *)animationView
{
    //放大
    CABasicAnimation *pulse = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    pulse.timingFunction= [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    pulse.duration = 0.5;
    pulse.repeatCount= 1;
    pulse.autoreverses= YES;
    pulse.fromValue= [NSNumber numberWithFloat:0.7];
    pulse.toValue= [NSNumber numberWithFloat:1.3];
    [animationView.layer addAnimation:pulse forKey:nil];
    
}
//抖动
- (void)shakeImageWithImageView:(UIImageView *)imageView {
    //创建动画对象,绕Z轴旋转
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    
    //设置属性，周期时长
    [animation setDuration:0.1];
    
    //抖动角度 1/180
    animation.fromValue = @(-M_1_PI/2);
    animation.toValue = @(M_1_PI/2);
    //重复次数，1次
    animation.repeatCount = 2;
    //恢复原样 结束后执行逆动画
    animation.autoreverses = YES;
    //锚点设置为图片中心，绕中心抖动
    imageView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    [imageView.layer addAnimation:animation forKey:@"rotation"];
}

//缩放
- (void)scaleAnimationWithImageView:(UIImageView *)imageView{
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.scale"];
    animation.values = @[@1.5 ,@0.8, @1.0,@1.2,@1.0];
    animation.duration = 0.5;
    animation.calculationMode = kCAAnimationCubic;
    [imageView.layer addAnimation:animation forKey:@"transform.scale"];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
