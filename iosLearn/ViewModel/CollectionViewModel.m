//
//  CollectionViewModel.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/10.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "CollectionViewModel.h"
#import "HttpHelper.h"
#import "ProductsModel.h"
#import "VSEasyAPIResponse.h"

@interface CollectionViewModel()
@end

@implementation CollectionViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}


#pragma mark - headerRefreshRequestWithCallback

- (void)headerRefreshRequestWithCallback:(callback)callback
{
    
    NSString *url = @"photos";
    
    
    [HttpHelper getWithPath:url params:nil success:^(NSDictionary *JSONDic) {
        
        NSMutableArray *productsModels=[NSMutableArray array];
        NSInteger code = VSEasyAPIResponseEnvelopeCode(JSONDic).integerValue;

        if (code == 1) {
            NSArray *productsArray=[JSONDic objectForKey:@"data"];

            for (NSDictionary *dic in productsArray) {
                ProductsModel *model=[[ProductsModel alloc] init];
                model.albumId = [dic objectForKey:@"albumId"];
                model.title = [dic objectForKey:@"title"];
                model.thumbnailUrl = [dic objectForKey:@"thumbnailUrl"];
                [productsModels addObject:model];
            }

            callback(productsModels);

        }else{
            NSLog(@"code != 1");
        }
        
        
        
    } failure:^(NSError *error) {
        
        NSLog(@"%ld",error.code);
        
    }];
    
}

@end
