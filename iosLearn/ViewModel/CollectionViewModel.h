//
//  CollectionViewModel.h
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/10.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef void (^callback) (NSArray *array);

@interface CollectionViewModel : NSObject

//collectionView头部刷新的网络请求
- (void)headerRefreshRequestWithCallback:(callback)callback;

@end

NS_ASSUME_NONNULL_END
