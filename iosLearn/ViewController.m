//
//  ViewController.m
//  iosLearn
//
//  Created by 李旭阳[产品技术中心] on 2020/6/9.
//  Copyright © 2020 李旭阳[产品技术中心]. All rights reserved.
//

#import "ViewController.h"
#import "CollectionViewController.h"
#import "TimerViewController.h"
#import "CommonViewController.h"
#import "MultithreadingViewController.h"
#import "LifeCycleViewController.h"
#import "BoundedBufferProblemViewController.h"
#import "NotificationControllerViewController.h"
#import "AnimationController.h"


@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //在这里增加条目，然后创建ViewController
    self.dataArray = @[@"工厂方法",@"CollectionView",@"NSTimer",@"多线程编程",@"UIViewController 的生命周期",@"生产者消费者",@"本地推送",@"核心动画"];
    [self setupUI];
}
- (void)setupUI{    
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    tableView.delegate = self;
    tableView.dataSource = self;
    //去除多余的横线
    tableView.tableFooterView = [[UIView alloc]init];
    [self.view addSubview:tableView];
    self.tableView = tableView;
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = self.dataArray[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        [self.navigationController pushViewController:[[CommonViewController alloc] init] animated:YES];
    }else if(indexPath.row == 1){
        [self.navigationController pushViewController:[[CollectionViewController alloc] init] animated:YES];
    }else if(indexPath.row == 2){
        [self.navigationController pushViewController:[[TimerViewController alloc] init] animated:YES];
    }else if(indexPath.row == 3){
        [self.navigationController pushViewController:[[MultithreadingViewController alloc] init] animated:YES];
    }else if(indexPath.row == 4){
        [self.navigationController pushViewController:[[LifeCycleViewController alloc] init] animated:YES];
    }else if(indexPath.row == 5){
        [self.navigationController pushViewController:[[BoundedBufferProblemViewController alloc] init] animated:YES];
    }else if(indexPath.row == 6){
        [self.navigationController pushViewController:[[NotificationControllerViewController alloc] init] animated:YES];
    }else if(indexPath.row == 7){
        [self.navigationController pushViewController:[[AnimationController alloc] init] animated:YES];
    }
}


@end
