//
//  UIImageView+custom.h
//  HCCiOSDemo
//
//  Created by 李旭阳[产品技术中心] on 2020/6/3.
//  Copyright © 2020 hao. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIImageView (custom)

@end

NS_ASSUME_NONNULL_END
